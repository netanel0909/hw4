#include "OutStream.h"
#include "Logger.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"

int main(int argc, char **argv)
{
	msl::os a;
	a << "hello";
	OutStream* q = OutStream();
	q << "I am the Doctor and I'm " << 1500 << " years old" << endline;

	OutStream o;
	o  << "1" << endline ;
	o << 2;

	printf("\n");
	FileStream f1;
	f1<<("test");
	f1.endline();
	f1.operator<<(3);

	printf("\n");

	OutStreamEncrypted o1;
	o1 << "hello" << 2 << endline;

	printf("\n");
	Logger l1;
	l1 << "os_print" << "hello";
	l1.setStartLine();
	l1 << "hello";
	l1.setStartLine();
	Logger l2;
	l2 << "first l2";
	l2.setStartLine();
	
	return 0;
}
