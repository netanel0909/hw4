#include "OutStreamEncrypted.h"



OutStreamEncrypted::OutStreamEncrypted()
{
	printf("please enter a number: \n");
	scanf("%d", &diff);
}

OutStreamEncrypted::OutStreamEncrypted(int n)
{
	diff = n;
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	int i = 0;
	while (str[i] != '\0')
	{
		if (str[i] < 127 && str[i] > 31)
		{
			fprintf(stdout, "%c", (str[i] - 32 + diff) % 95 + 32);
		}
		else
		{
			fprintf(stdout, "%c", str[i]);
		}
		i++;
	}		
	return *this;
}


OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	int i = 0;
	char res[50] = { 0 };
	sprintf(res, "%d", num);
	while (res[i] != '\0')
	{
		res[i] = (res[i] - 32 + diff) % 95 + 32;
		i++;
	}
	fprintf(stdout, res);
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)())
{
	pf();
	return *this;
}

void endline()
{
	printf("\n");
}
