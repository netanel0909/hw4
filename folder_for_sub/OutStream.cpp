#include "OutStream.h"
#include <stdio.h>

OutStream::OutStream()
{
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(stdout, str);
	return *this;
}


OutStream& OutStream::operator<<(int num)
{
	char res[50] = { 0 };
	sprintf(res, "%d", num);
	fprintf(stdout, res);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}
