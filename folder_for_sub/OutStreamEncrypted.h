#include "OutStream.h"
#pragma once
class OutStreamEncrypted:OutStream
{
	int diff;
public:
	OutStreamEncrypted();
	OutStreamEncrypted(int n);
	~OutStreamEncrypted();
	virtual OutStreamEncrypted& operator<<(const char *str);
	virtual OutStreamEncrypted& operator<<(int num);
	virtual OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)());
};

