#pragma once
#include<stdio.h>
#include<stdlib.h>
#include <string.h>
class OutStream
{
protected:
	FILE * F;
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
};

void endline();