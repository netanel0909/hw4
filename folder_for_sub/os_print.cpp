#include "os_print.h"

namespace msl
{
	os_print::os_print()
	{
	}

	os_print::~os_print()
	{
	}

	os_print& os_print::operator<<(const char *str)
	{
		printf("%s", str);
		return *this;
	}

	os_print& os_print::operator<<(int num)
	{
		printf("%d", num);
		return *this;
	}

	os_print& os_print::operator<<(void(*pf)())
	{
		pf();
		return *this;
	}


	void endline()
	{
		printf("\n");
	}
}