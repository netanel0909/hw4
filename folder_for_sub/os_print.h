#pragma once
#include<stdio.h>
#include <string.h>
using namespace std;
namespace msl
{
	class os_print
	{
	protected:
		FILE* file;
	public:
		os_print();
		~os_print();

		virtual os_print& operator<<(const char *str);
		virtual os_print& operator<<(int num);
		virtual os_print& operator<<(void(*pf)());
	};

	void endline();
}