#include "Logger.h"

void Logger::setStartLine()
{
	printf("\n");
	_startLine = true;
	static unsigned int count = 0;
	count++;
	printf("number of times log used by now: %d", count);
	printf("\n");
}

Logger::Logger()
{
	_startLine = true;
}

Logger::~Logger()
{
}

Logger & operator<<(Logger & l, const char * msg)
{
	if (l._startLine)
	{
		l.os << "LOG: " << msg;
		l._startLine = false;
	}
	else {
		l.os << msg;
	}
	return l;
}

Logger & operator<<(Logger & l, int num)
{
	if (l._startLine)
	{
		l.os << "LOG: " << num;
	}
	else {
		l.os << num;
	}
	return l;
}

Logger & operator<<(Logger & l, void(*pf)())
{
	pf();
	return l;
}

